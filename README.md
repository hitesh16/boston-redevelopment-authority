# Document Center Enhancement
##Instructions
This [Document Center](http://www.bostonredevelopmentauthority.org/document-center) experience is used by the public to search through documents, records, and other files available online.  The goal is to entirely rethink the left hand search experience and recreate one using best practices in information retrieval, user experience, and interface. Please mockup, develop, or create a new search experience.  

##Technology
Please use Javascript, CSS, and Bootstrap where you can.

##Additional Details

* Better search experiences can be found here: http://www.bostonredevelopmentauthority.org/planning/planning-initiatives
* Comment your code and include notes of best practices you've found elsewhere.
* Your product does not have to work; be prepared to explain what your direction was
* When possible, use validation.

##Submission
Please package the HTML, CSS, and any supporting files in to your version of the repo. Include a readme on what your process was.
Don't forget to comment your code. =)

Please use a [pull request](https://confluence.atlassian.com/bitbucket/fork-a-repo-compare-code-and-create-a-pull-request-269981804.html) to submit your submissions. To be fair to all applicants, no late submissions will be accepted.  Thank you!

##FAQ
* **When should we have the assignment submitted?**  
Friday, Dec 4, 2015. To be fair to all applicants, no late submissions will be accepted.  Please, feel free to send partial or incomplete submissions.   We are gauging your current development ability and will have to reschedule the interview if we don't receive it.

* ** Can I submit early?  **
Absolutely!  This will guarantee an early interview with us too. 

* **How do I schedule an interview?  **
Interviews are scheduled 'first-come, first-serve' based on the time of your submission.  If you get the submission to us soon, we will schedule an interview.  Interviews will **not** be scheduled without a submission first.

* **How should we submit the project?**  
Please package the HTML, CSS, and any supporting files in a zip. Include a readme on what your process was. Submit your assignment using the Git [pull request](https://confluence.atlassian.com/bitbucket/fork-a-repo-compare-code-and-create-a-pull-request-269981804.html) feature.

* **Where is the interview?**  
Please check in with the reception on the 9th floor of City Hall (1 City Hall Square, Boston, MA). We urge you to take public transportation. State Street station is closest.

* **What technologies can I use for the assignment?**  
Your weapon of choice.  But be ready to explain why!